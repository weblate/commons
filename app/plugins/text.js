import marked from 'marked';
import deburr from 'lodash/deburr';

const methods = {
  md2html(str) {
    let formatted = str;
    if (typeof str === 'string' && !/^https?:\/\//.test(str)) {
      formatted = (/\n\n/.test(formatted))
        ? marked(formatted)
        // marked convert single string to paragraphs
        // or to ordered list
        // so cleaning is needed
        : marked(formatted)
          .replace(/^<p>([\S\s]*?)<\/p>\n?$/m, '$1')
          .replace(/^<ol([\S\s]*?)>\n<li>([\S\s]*?)<\/li>\n<\/ol>\n?$/m,
            (c, $1, $2) => `${$1 ? $1.replace(/[^0-9]/g, '') : '1'}. ${$2}`);
    }
    return formatted;
  },

  textTransform(str, options) {
    let formatted = str;
    if (options !== '') {
      // -t or text (html cleanup)
      if (/text/.test(options) || /^-.*?t.*?$/.test(options)) {
        const tmp = new DOMParser().parseFromString(formatted, 'text/html');
        formatted = tmp.body.textContent || '';
      }
      // -L or latin (remove diacritics)
      if (/latin/.test(options) || /^-.*?L.*?$/.test(options)) {
        formatted = deburr(formatted);
      }
      // -l (lowercase)
      if (/^-.*?l.*?$/.test(options)) {
        formatted = formatted.toLowerCase();
      }
      // -U (uppercase)
      if (/^-.*?U.*?$/.test(options)) {
        formatted = formatted.toUpperCase();
      }
      // -K (capitalize)
      if (/^-.*?K.*?$/.test(options)) {
        formatted = formatted.replace(/^./, v => v.toUpperCase());
      }
      if (/sanitize/.test(options) || /^-.*?@.*?$/.test(options)) {
        formatted = formatted.toLowerCase().trim()
          .replace(/@:[.a-z]+ /g, '') // remove vue-i18n var
          .replace(/[ '’]/g, '-')
          .replace(/[^a-zA-Z0-9-_.]/g, '');
      }
      if (/noframa/.test(options) || /^-.*?~.*?$/.test(options)) {
        formatted = formatted.replace('framand', 'and')
          .replace('framage', 'age')
          .replace('framae', 'mae')
          .replace('framin', 'min')
          .replace('frame', 'me')
          .replace('frama', '')
          .replace('.', '');
      }
    }
    return formatted;
  },

  $tf(key, locale, values, i18n) {
    if (i18n === undefined) {
      i18n = this.$i18n;
    }
    let options = locale || '';
    let trueLocale = i18n.t('lang');
    let tplvars = undefined;

    if (Array.isArray(values) || typeof values === 'object') {
      tplvars = values;
    }

    if (typeof locale === 'string') {
      // Split locale and options
      if (i18n.t('available', 'locales').indexOf(locale.split(' ')[0]) > 0) {
        // locale = 'en_GB -t', 'fr_FR latin'…
        [trueLocale] = [locale.split(' ')[0]];
        options = locale.substring(locale.indexOf(' ') + 1);
      }
    } else {
      // "locale" is missing so, in fact, it is a "value"
      tplvars = tplvars || locale;
    }

    let formatted = key || '';
    if (!/^-.*?k.*?$/.test(options)) {
      // -k means we only want to keep the key or text/html to format
      if (/^-.*?m.*?$/.test(options)) {
        // -m means we want to translate from the English text
        formatted = i18n.t(
          i18n.t(`map['${JSON.stringify(formatted.trim().replace(/\n[ ]+/g, '\n'))}']`, 'locales'),
          trueLocale,
          tplvars
        );
      } else {
        formatted = i18n.t(formatted, trueLocale, tplvars);
      }
    }

    if (typeof formatted === 'string') {
      formatted = formatted
        // Translate :emoji:
        .replace(/:((?!.*--.*|.*__.*)[a-z0-9_-]+?):/g, (c, $1) => {
          if (i18n.te(`emoji['${$1}']`)) {
            return i18n.t(`emoji['${$1}']`);
          }
          return `:${$1}:`;
        })
        // Translate linked @:key or @:(key)
        // (cf linkKeyMatcher in vue-i18n/src/index.js)
        .replace(/@:\(?([\w\-_|.]+[\w\-_|]+)\)?/g, (c, $1) => i18n.t($1));
    }

    formatted = this.md2html(formatted);

    return this.textTransform(formatted, options);
  },

};

export default {
  md2html: methods.md2html,
  textTransform: methods.textTransform,
  $t: methods.$tf, // send to index.js override $t()
  install(Vue) {
    Vue.mixin({
      methods,
    });
  },
};
